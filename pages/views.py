from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def homeview(request):
    return render(request, "home.html")
def contactview(request):
    return render(request , "contacts.html")
def aboutview(request):
    return render(request , "about.html")
