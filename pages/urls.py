from django.urls import path
from .views import homeview,contactview,aboutview

urlpatterns = [
    path("", homeview),
    path("home/", homeview),
    path("contacts/", contactview),
    path("about/", aboutview)
]